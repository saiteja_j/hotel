## Hotel Reservation SpringBoot Project ##

## Build ##
`mvn clean install`
## Run Service ##
`mvn spring-boot:run`
## Sanple Input/Output ##


                1. POST localhost:8080/reservation
                     Request Body:
                     {
                         "hotelName": "taj",
                         "roomNumber": 250
                     }
                     
                     Response: 
                     Status: 200
                     Body:
                     {
                        true
                     }
                 
                 2. POST   localhost:8080/reservation
                 
                     Request Body:
                     {
                         "hotelName": "taj",
                         "roomNumber": 250
                     }
                     
                     Response: 
                     Status: 409
                     Body:
                     {
                        false
                     }
                 
                 3. POST   localhost:8080/reservation
                 
                     Request Body:
                     {
                         "hotelName": "TAJ",
                         "roomNumber": 490
                     }
                     
                     Response: 
                     Status: 200
                     Body:
                     {
                        true
                     }
                 
                4. POST   localhost:8080/reservation
                 
                     Request Body:
                     {
                         "hotelName": "Taj",
                         "roomNumber": 490
                     }
                     
                     Response: 
                     Status: 409
                     Body:
                     {
                        false
                     }
                 
                 5. POST   localhost:8080/reservation
                 
                     Request Body:
                     {
                         "hotelName": "Radison",
                         "roomNumber": 490
                     }
                     
                     Response: 
                     Status: 200
                     Body:
                     {
                        true
                     }


